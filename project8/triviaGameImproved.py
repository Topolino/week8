# Name: Joey Carberry
# Date: October 19, 2015
# Project: Project8 - Trivia Game using a text document
file = open('questionsAndAnswers.txt', 'r')
file2 = open('answers.txt', 'r')
lines = file.readlines()
lines2 = file2.readlines()
def gameLoop():
    # Targets and prints the questions and answers
    start = 0
    z = 4
    nC = 0
    for x in range(0,11):
        num = 1
        sx = lines[x]
        sxLen = sx.__len__() - 1
        print ('Question:', sx[0:sxLen])
        end = start + 4
        for y in range(start,end):
            sy = lines2[y]
            syLen = sy.__len__() - 1
            print (num ,'.)', sy[0:syLen])
            num += 1
        start += 5
        userAns = int(input('Answer: '))
        correctAnswer = int(lines2[z])
        if (userAns == correctAnswer):
            print('Correct Answer!')
            nC += 1
            print('Number correct:', nC)
        else:
            print('Incorrect Answer')
            print('Number correct:', nC)
        if (z == 24):
            print ('Player 2\'s turn!')
        z += 5


gameLoop()